<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */

/**
 * Add your routes here
 */
$app->get('/', function () {
    echo $this['view']->render('index');
});

$app->post('/', function () use ($app) {

    $login    = $app->request->get('login');
    $password = $app->request->get('password');

    $send_data = [
        'method' => 'login',
        'params' => [
            'login'    => $login,
            'password' => $password
        ]
    ];

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, "http://admin");
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-type: application/json'
        )
    );
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($send_data));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    $data = json_decode(curl_exec($curl));

    curl_close($curl);

    echo $this['view']->setVars([
        'message' => $data->result ? $data->message : $data->error->message
    ])->render('index');
});

/**
 * Not found handler
 */
$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo $app['view']->render('404');
});
