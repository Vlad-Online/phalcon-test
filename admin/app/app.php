<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */

/**
 * Add your routes here
 */
$app->post('/', function () use ($app) {
    $jsonData = $app->request->getJsonRawBody(true);
    $user     = User::findFirstByLogin($jsonData['params']['login']);
    if ($user && $app->security->checkHash($jsonData['params']['password'], $user->password)) {
        $data['result']  = true;
        $data['message'] = 'Успешная авторизация!';
    } else {
        $data['result'] = null;
        $data['error']  = [
            'code'    => 404,
            'message' => 'Неверный логин или пароль!'
        ];
    }

    $response = new \Phalcon\Http\Response();
    $response->setJsonContent($data);

    return $response;
});

/**
 * Not found handler
 */
$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo $app['view']->render('404');
});
