<?php

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class UsersMigration_103
 */
class UsersMigration_103 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('users', [
                'columns' => [
                    new Column('id', [
                        'type'          => Column::TYPE_INTEGER,
                        'size'          => 10,
                        'notNull'       => true,
                        'autoIncrement' => true,
                        'primary'       => true,
                    ]),
                    new Column(
                        'login',
                        [
                            'type'    => Column::TYPE_VARCHAR,
                            'size'    => 30,
                            'notNull' => true,
                        ]
                    ),
                    new Column(
                        'password',
                        [
                            'type'    => Column::TYPE_VARCHAR,
                            'size'    => 60,
                            'notNull' => true,
                        ]
                    ),
                ],
            ]
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
        $security = new Phalcon\Security;
        self::$connection->insert(
            'users',
            [
                'admin',
                $security->hash('admin')
            ],
            [
                'login',
                'password'
            ]
        );
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
